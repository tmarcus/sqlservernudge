﻿/*
 * Copyright (c) 2015 by Trey Marcus, Electron Consulting, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
*/

using System;
using System.Data.SqlClient;

namespace SQLServerNudge
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine();
                string TableName = System.Configuration.ConfigurationManager.AppSettings["TableName"];
                if (TableName == null || TableName == "")
                {
                    Console.WriteLine("TableName setting must be specified.");
                    return;
                }

                using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    connection.Open();

                    if (args.Length == 1 && args[0].ToLower() == "count")
                    {
                        command.CommandText = "SELECT COUNT(*) FROM " + TableName;
                        Console.WriteLine("Number of rows in " + TableName + " table: " + command.ExecuteScalar().ToString());
                    }
                    else
                    {
                        command.CommandText = "SELECT TOP 1 * FROM " + TableName;
                        command.ExecuteScalar();
                        Console.WriteLine("Nudged");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }
        }
    }
}
