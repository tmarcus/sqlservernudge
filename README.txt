﻿SQLServerNudge

Copyright (c) 2015 by Trey Marcus, Electron Consulting, Inc.

SQLServerNudge is a basic command line application which simply makes a tiny call to a Database. This is useful if
you are using SQL Server Express which will go idle after approximately 20 minutes of no usage. When SQL Server
Express goes idle, the next request wakes it up without any issues, but this could take around 10-15 seconds to
complete, which could be annoying for a web visitor having to wait. It is suggested to set this program up as a
scheduled task directly on the server where SQL Server Express resides. The suggested interval is every 15 minutes.

SETUP:
1. Open the configuration file (SqlServerNudge.exe.config). For DefaultConnection, specify the proper Server (the
   default is ".\SQLEXPRESS") and Database name. Change the value of the setting called TableName to the name of a
   table in your DB that you want to use. Typically this would be a small table.
2. By default, the connection string is using Trusted_Connection=True, which is the preferred option because it is
   considered more secure than specifying a username and password in the config file (unless you encrypt it). You
   will hovever need to run this program with a user that has access to SQL Server. For web servers you typically
   would use the same user that the IIS application pool uses, or you could create a new user with read-only access
   specifically for this application.

USAGE:
Once your config file is setup properly, simply run the program. If there is an error connecting to the DB a message
will be displayed. If the program works, only the word "Nudged" will be printed. Optionally, you can specify a
single argument with the word "count" which will display the number of rows in the table it is querying. This is a
good way to test to make sure the program is working properly. Once you have confirmed the proper usage, setup your
scheduled task to run the program every 15 minutes, without the count argument.

Note: the count argument uses slightly more resources than without it, which is why I recommend only using the count
option for verification. In a small table though the difference would be neglibile. You'd probably need more than at
least 10 million rows in the table before the count argument would put even a noticeable load on the DB.

The code is released under the MIT Licence. (Basically you can do anything you want with it or to it, as long as you
include the original copyright with it.)

Compiled executable size: 6,144 Bytes
